// const express = require('express')
// const kafka = require('kafka-node')
// const app = express()
// const mongoose = require('mongoose')

// app.use(express.json())

// // Kafka Connection, Producer and Topic
// const databasesAreRunning = async () => {

//     mongoose.connect(process.env.MONGO_URL)
//     const User = new mongoose.model('user', {
//         name: String,
//         email: String,
//         password: String
//     })

//     const client = new kafka.KafkaClient({kafkaHost: process.env.KKAFKA_BOOTSTRAP_SERVERS})
//     const consumer = new kafka.Consumer(client, [{topic: process.env.KAFKA_TOPIC}], {autoCommit: false})

//     consumer.on('message', async (message) => {
//         console.log('Consumer is Fired')
//         const user = await new User(JSON.parse(message.value))
//         await user.save()
//     })

//     consumer.on('error', async (err) => {
//         console.log('Consumer Error: ', err)
//     })
// }
// setTimeout(databasesAreRunning, 15000)

// app.listen(process.env.PORT)



const express = require('express');
const kafka = require('kafka-node');
const app = express();
const mongoose = require('mongoose');

app.use(express.json());

// Kafka Connection, Producer and Topic
const databasesAreRunning = async () => {
  try {
    await mongoose.connect(process.env.MONGO_URL);
    const userSchema = new mongoose.Schema({
      name: String,
      email: String,
      password: String,
    });
    const User = mongoose.model('user', userSchema);

    const client = new kafka.KafkaClient({
      kafkaHost: process.env.KAFKA_BOOTSTRAP_SERVERS,
    });
    const consumer = new kafka.Consumer(
      client,
      [{ topic: process.env.KAFKA_TOPIC }],
      {
        autoCommit: false,
        groupId: 'test-group',
      }
    );

    consumer.on('message', async (message) => {
      console.log('Consumer is Fired');
      const user = new User(JSON.parse(message.value));
      await user.save();
    });

    consumer.on('error', async (err) => {
      console.log('Consumer Error: ', err);
    });
  } catch (err) {
    console.error(err);
  }
};
setTimeout(databasesAreRunning, 15000);

app.listen(process.env.PORT);

// const express = require('express')
// const kafka = require('kafka-node')
// const app = express()
// const sequelize = require('sequelize')

// app.use(express.json())

// // Kafka Connection, Producer and Topic
// const databasesAreRunning = async () => {

//     const db = new sequelize(process.env.POSTGRES_URL)
//     const User = db.define('user', {
//         name: sequelize.STRING,
//         email: sequelize.STRING,
//         password: sequelize.STRING
//     })
//     db.sync({ force: true })
//     const client = new kafka.KafkaClient({kafkaHost: process.env.KKAFKA_BOOTSTRAP_SERVERS})
//     const producer = new kafka.Producer(client)
//     producer.on('ready', async () => {
//         console.log('producer is ready')
//         app.post('/', async (req, res) => {
//             producer.send([
//                 {
//                     topic: process.env.KAFKA_TOPIC,
//                     message: JSON.stringify(req.body)
//                 }
//             ], async (err, data) => {
//                 if(err){
//                     console.log(err)
//                 }else{
//                     console.log(data)
//                     await User.create(req.body)
//                     res.send(res.body)
//                 }
//             })
//         })
//     })
// }
// setTimeout(databasesAreRunning, 10000)

// app.listen(process.env.PORT)

const express = require('express');
const kafka = require('kafka-node');
const app = express();
const Sequelize = require('sequelize');

app.use(express.json());

// Kafka Connection, Producer and Topic
const databasesAreRunning = async () => {

  const db = new Sequelize(process.env.POSTGRES_URL);
  const User = db.define('user', {
    name: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING
  });

  try {
    await db.authenticate();
    console.log('Connection with database has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
    return;
  }

  await db.sync({ force: true });

  const client = new kafka.KafkaClient({ kafkaHost: process.env.KAFKA_BOOTSTRAP_SERVERS });
  const producer = new kafka.Producer(client);
  producer.on('ready', async () => {
    console.log('producer is ready');
    app.post('/', async (req, res) => {
      producer.send([
        {
          topic: process.env.KAFKA_TOPIC,
          messages: JSON.stringify(req.body)
        }
      ], async (err, data) => {
        if (err) {
          console.log(err);
          res.status(500).send(err);
        } else {
          console.log(data);
          await User.create(req.body);
          res.send(req.body);
        }
      });
    });
  });
};

databasesAreRunning();

app.listen(process.env.PORT);
